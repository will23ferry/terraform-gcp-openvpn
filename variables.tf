variable "region" {
    description = "change this if desired - support for terraform docs is a WIP"
    default = "us-central1"
}

variable "zone" {
    description = "change this if desired - support for terraform docs is a WIP"
    default = "us-central1-a"
}

variable "project" {
    description = "--- change this --- - support for terraform docs is a WIP"
    default = "example-project-name"
}

variable "private_key_file" {
    description = "--- change --- path and private key name to point to the key you want added as metadata on GCE instance"
    default = "~/.ssh/placeholderkeyname"   # see readme for key generation commands
}

variable "public_key_file" {
    description = "--- change ---  path and public key name to point to the key you want added as metadata on GCE instance"
    default = "~/.ssh/placeholderkeyname.pub"   # see readme for key generation commands
}

variable "client_config_path" {
    description = "directory for SCP initial client config over"
    default = "../client_configs"
}

variable "client_config_name" {
    description = "this value should remain static for now (commands in remote-exec provisioner that moves file need to be updated to support variable) - support for terraform docs is a WIP"
    default = "client"
}

variable "cert_details" {
    description = "file with defaults for on-server EasyRSA Cert Authority signing - support for terraform docs is a WIP"
    default = "./cert-info"
}

variable "remote_user" {
  description = "The user to operate as on the VM. SSH Key is generated for this user"
  default     = "ubuntu"
}
