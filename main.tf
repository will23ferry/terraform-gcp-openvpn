provider "google" {
  project = var.project
  region  = var.region
}

# recommend newest version of Terraform, however this is tested working on 0.12 - 1.0
terraform {
  required_version = ">= 0.12"
}

# instructions regarding backend will be added to readme
# currently, backend is defined and provisioned into GCP using ArgoCD
terraform {
  backend "gcs" {
    bucket  = "ovpn-backend"
    prefix  = "terraform/state"
  }
}
