
# create a vpc network, autocreate subnet
resource "google_compute_network" "ovpn-vpc" {
  name                    = "ovpn-vpc"
  auto_create_subnetworks = "true"
}


# create a firewall, rules, and tags
resource "google_compute_firewall" "ovpn-firewall" {
  name    = "ovpn-firewall"
  network = google_compute_network.ovpn.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  allow {
    protocol = "udp"
    ports    = ["1194"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["ovpn", "allow-ssh", "http-server", "https-server"]
}


# create a named and static external IP address
resource "google_compute_address" "ovpn-external-address" {
    name = "ovpn-external-address"
    address_type = "EXTERNAL"
    region       = var.region
}
