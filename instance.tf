
resource "google_compute_instance" "ovpn-server" {
  name         = "ovpn-server"
  machine_type = "n1-standard-1"
  zone         = var.zone


  tags = ["ovpn-udp", "https-server", "http-server"]

  boot_disk {
    initialize_params {
      # set image to data section at bottom of file
      image = data.google_compute_image.ubuntu.self_link
    }
  }


  # set IP forwarding to true for VPN
  can_ip_forward = true

  network_interface {
    network = google_compute_network.ovpn-vpc.name

    # set IP address to named-static address (prepared for DNS)
    access_config {
        nat_ip = google_compute_address.ovpn-external-address.address
    }
  }


  service_account {
    scopes = ["https://www.googleapis.com/auth/compute.readonly"]
  }

  # add ssh keys to metadata when starting instance up so ssh can be used to remote-exec
  metadata = {
    startup-script = data.template_file.deployment_shell_script.rendered
    sshKeys        = "ubuntu:${file(var.public_key_file)}"
  }


  provisioner "remote-exec" {
    inline = [
      "echo 'Scheduling instance reboot in one minute ...'",
      "sudo shutdown -r +1",
    ]


    connection {
      host        = google_compute_instance.ovpn-server.network_interface[0].access_config[0].nat_ip
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.private_key_file)
      timeout     = "5m"
    }
  }


  provisioner "remote-exec" {
    inline = [
      "echo 'Waiting for client config ...'",
      "sudo cp /root/client1.ovpn /home/ubuntu/test-client.ovpn",
      "while [ ! -f /home/ubuntu/test-client.ovpn ]; do sleep 5; done",
      "echo 'DONE!'",
    ]


    connection {
      host        = google_compute_instance.ovpn-server.network_interface[0].access_config[0].nat_ip
      type        = "ssh"
      user        = "ubuntu"
      private_key = file(var.private_key_file)
      timeout     = "5m"
    }
  }

  # 
  depends_on = [
      google_compute_address.ovpn-external-address,
  ]

  #   provisioner "local-exec" {
  #   command = "scp -o StrictHostKeyChecking=no -i ${var.private_key_file} ubuntu@${google_compute_instance.ovpn.network_interface[0].access_config[0].nat_ip}:/home/ubuntu/test-client.ovpn ${var.client_config_path}/${var.client_config_name}.ovpn"
  # }
}


# create a vpc network, autocreate subnet
resource "google_compute_network" "ovpn-vpc" {
  name                    = "ovpn-vpc"
  auto_create_subnetworks = "true"
}


# create a firewall, rules, and tags
resource "google_compute_firewall" "ovpn-firewall" {
  name    = "ovpn-firewall"
  network = google_compute_network.ovpn.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  allow {
    protocol = "udp"
    ports    = ["1194"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["ovpn", "allow-ssh", "http-server", "https-server"]
}


# create a named and static external IP address
resource "google_compute_address" "ovpn-external-address" {
    name = "ovpn-external-address"
    address_type = "EXTERNAL"
    region       = var.region
}


# instance image type
data "google_compute_image" "ubuntu" {
  family  = "ubuntu-2004-lts"
  project = "ubuntu-os-cloud"
}


# shell script and vars
data "template_file" "deployment_shell_script" {
  template = file("init.sh")

  vars = {
    cert_details       = file(var.cert_details)
    client_config_name = var.client_config_name
  }
}
