# print public ip out
output "public_ip" {
  value = "VPN IP Address: ${google_compute_instance.ovpn-server.network_interface[0].access_config[0].nat_ip}"
}
